## Author: Amy Reichhold, areichh2@uroegon.edu ##

## control calculation ##
According to the below website, the largest controle distance on the table is 1000 and the smallest window is 0-200. While the controle distance is greater than 200, check and see how big the controle distance is starting at 1000. If it's greater than 1000 divide the controle distance exceeding 1000 by the minimum or maximum speed for 1000(13.333 or 26, respectively), and add it to a running total. Subtract the controle distance that is exceeding 1000 from the original controle distance. Repeat this for the smaller windows until the remaining controle distance is less than 200. The resulting running total is the controle opening or closing time.

If the controle distance is one of several predetermined brevet distances, then the official rules state the specific closing time which overrides the above running total.

https://rusa.org/pages/acp-brevet-control-times-calculator