import math

"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

def ambiguous_opening_time(dist):
    running_total = 0
    while dist > 200:
        if dist > 1000:
            running_total += (dist - 1000) / 26
            dist -= dist - 1000
        elif dist > 600:
            running_total += (dist - 600)/28
            dist -= dist - 600
        elif dist > 400:
            running_total += (dist - 400)/30
            dist -= dist - 400
        else:
            running_total += (dist - 200)/32
            dist -= dist - 200
    running_total += dist/34
    return running_total

def ambiguous_closing_time(dist):
    running_total = 0
    while dist > 200:
        if dist > 1000:
            running_total += (dist - 1000) / 13.333
            dist -= dist - 1000
        elif dist > 600:
            running_total += (dist - 600)/11.428
            dist -= dist - 600
        elif dist > 400:
            running_total += (dist - 400)/15
            dist -= dist - 400
        else:
            running_total += (dist - 200)/15
            dist -= dist - 200
    running_total += dist/15
    return running_total


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    
"""
    open_t_dec = ambiguous_opening_time(control_dist_km)
    start_time = arrow.get(brevet_start_time)
    print("start time: ", brevet_start_time)
    hour = math.floor(open_t_dec)
    mins = round((open_t_dec - hour) * 60)
    shifted = start_time.shift(minutes=mins,hours=hour)
    return shifted.isoformat()

def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    start_time = arrow.get(brevet_start_time)
    #control_times = arrow.get(brevet_start_time)
    if control_dist_km == 0.0:
        return start_time.shift(hours=+1).isoformat()
    elif control_dist_km == 200.0 and brevet_dist_km == 200:
        return start_time.shift(hours=+13,minutes=+30).isoformat()
    elif control_dist_km == 300.0 and brevet_dist_km == 300:
        return start_time.shift(hours=+20).isoformat()
    elif control_dist_km == 400.0 and brevet_dist_km == 400:
        return start_time.shift(hours=+27).isoformat()
    elif control_dist_km == 600.0 and brevet_dist_km == 600:
        return start_time.shift(hours=+40).isoformat()
    elif control_dist_km == 1000.0 and brevet_dist_km == 1000:
        return start_time.shift(hours=+75).isoformat()
    else:
        close_t_dec = ambiguous_closing_time(control_dist_km)
        start_time = arrow.get(brevet_start_time)
        print("start time: ", brevet_start_time)
        hour = math.floor(close_t_dec)
        mins = round((close_t_dec - hour) * 60)
        shifted = start_time.shift(minutes=mins,hours=hour)
        return shifted.isoformat()

